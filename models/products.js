const mongoose = require("mongoose");

let productSchema = new mongoose.Schema(
	{
		name:{
			type:String,
			require:true,
			max:100
		},
		price:{
			type:Number,
			required:true
		}
	})

module.exports = mongoose.model("Product", productSchema)