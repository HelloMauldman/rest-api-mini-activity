const Product = require("../models/products");

module.exports.addProducts = (requestBody) => {
	return Product.findOne({name: requestBody.name}).then((result, error)=>{
		if(result !== null && result.name == requestBody.name){
			return "Already existing."
		}
		else{
			let newProduct = new Product({
				name: requestBody.name,
				price: requestBody.price
			})
			return newProduct.save().then((savedProd, savedErr)=>{
				if(savedErr){
					console.log(error);
					return false;
				}
				else{
					return savedProd;
				}
			})
		}
	})

	//
	// let newProduct = new Product({
	// 	name: requestBody.name,
	// 	price: requestBody.price
	// })
	// return newProduct.save().then((products, error)=>{
	// 	if(error){
	// 		console.log(error);
	// 		return false;
	// 	}
	// 	else{
	// 		return products;
	// 	}
	// })
}
	

module.exports.getAllProducts = () =>{
	return Product.find({})
}


module.exports.getProduct = (productId) => {
	return Product.findById(productId).then((findProd, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return findProd;
		}
	})
}

module.exports.deleteProduct = (productId) =>{
	return Product.findByIdAndRemove(productId).then((removedProd, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return removedProd;
		}
	})
}