const express = require("express");
const mongoose = require("mongoose");
const productRoute = require("./routes/productRoute")


//server setup
const app = express();
const port = 3001;

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose connect
mongoose.connect("mongodb+srv://admin:admin1234@zuitt-bootcamp.exnj7.mongodb.net/REST-API-mini-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

app.use("/products", productRoute)


//
app.listen(port,()=>{console.log(`Server is running at port ${port}`)})