const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")

//POST
router.post("/", (req, res)=>{
	productController.addProducts(req.body).then(result=>res.send(result))
})


//GET
router.get("/",(req, res)=>{
	productController.getAllProducts().then((result)=>{
		res.send(result)
	})
})
//GET/1

router.get("/:id", (req, res)=>{
	productController.getProduct(req.params.id).then((result)=>{res.send(result)})
})


//DELETE

router.delete("/delete/:id", (req, res)=>{
	productController.deleteProduct(req.params.id).then((result)=>{res.send(result)});
})


module.exports = router;